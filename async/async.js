module.exports = {
  // This is expected to run sequentially. The data from the first operation
  // is passed to the data on the second operation
  // (check out the user.name.toUpperCase() call).
  sequence: function(functions) {
    // Let's keep track of where we are as we step through the functions
    var index = 0;
    return function(callback) {
      var step = function(err, data) {
        if (index == functions.length) {
          return callback(null, data);
        }
        // 1. Pass the function as the first argument (callback)
        // 2. The second argument is manipulated, then passed on.
        functions[index++](step, data);
      }
      step(null, functions[index]);
    };
  },
  // So this one wants us to run two processes in parallel,
  // then merge the results.
  parallel: function(functions) {
    // We'll decrement this each time we receive a response.
    var waiting = functions.length;
    // Let's store all the values gathered in parallel
    var returns = [];
    return function(callback) {
      // Create the completed callback
      var completed = function(err, data) {
          // Merge the data together
          returns.push(data);
          waiting--;
          if (waiting == 0) {
            return callback(null, returns);
          }
        }
        // Iterate and dispatch functions.
      functions.forEach(function(func) {
        func(completed, arguments);
      });
    }
  },
  // First completing function wins.
  race: function(functions) {
    // Let's flag whether or not we have a winner.
    var winner = false;
    return function(callback) {
      var finished = function(err, data) {
          // If there's not a winner -- You're the winner!
          if (!winner) {
            winner = true;
            return callback(null, data);
          }
          // If not, just return, you poor thing.
          return;
        }
        // JavaScript for loops are non-blocking.
      functions.forEach(function(func) {
        func(finished, arguments);
      });
    }
  }
};

var throttlePromises = function(limit, promises) {
  // Return a Promise that recurses through all of our
  // promises and resolves when all of the throttled
  // promises are done.
  return new Promise(function(resolve) {
    // We're going to keep track of how many are running
    // and compare that to the limit given in the construction
    // We're also going to keep track of all our results
    var running = 0,
      results = [];
    // This captures the original size of the promise list.
    var expected = promises.length;

    function next() {
      // If we still have promises...
      if (promises.length > 0) {
        // and the limit hasn't been reached...
        if (running < limit) {
          // Spin up a thread.
          running++;
          // Capture the position
          var index = expected - promises.length;
          var run = promises.pop();
          run().then(function(result) {
            results[index] = result; // based on position
            running--;
            next(); // This will keep us at 5
          });

          // This will get us up to 5
          if (running < limit) {
            next();
          }
        }
      }

      // If we have resolved everything, and there are no more
      // promises left, call the major resolve and inject results.
      if (running == 0 && promises.length == 0) {
        resolve(results);
      }
    }

    // kick off the process
    next();
  });
};

module.exports = throttlePromises;

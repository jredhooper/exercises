var throttle = function(func, milliseconds) {
  // Throttle flag
  var throttled = false;
  var surplus, start;

  // I started here. Just had to add a flag that preserves the state
  // of the program.
  var run = function(ctx, args) {
    throttled = true;
    // Call apply with the collected context/args
    func.apply(ctx, args);
  }

  return function() {
    // I added this afterwards – this is to ensure that the program's
    // throttling starts _after_ the first throttle() has been called.
    if (!start) {
      start = setInterval(function() {
        throttled = false;
        if (surplus) {
          surplus();
          delete surplus;
        }
      }, milliseconds);
    }

    // Grab the context
    // Grab the arguments
    var ctx = this;
    var args = arguments;
    // If it's not throttled, go right now!
    if (!throttled) {
      run(ctx, args);
    } else {
      // If it is throttled, set the surplus
      // and then execute the queued surplus when the interval comes around
      surplus = function() {
        run(ctx, args);
      };
    }
  };
};

module.exports = throttle;

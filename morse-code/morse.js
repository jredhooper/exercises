var transmitter = function(options, callback) {
  var LEN = {
    DOT: 1,
    DASH: 3,
    CHARACTER: 3,
    WORD: 7
  };


  var characters = options.message.split('');
  var signals = [];
  var morse = [];

  var spacing = false;

  // Digest message into morse code state
  for (var c = 0; c < characters.length; c++) {


    // grab current character
    var char = characters[c];


    if (c != 0) // don't add spaces to start
      if (options.codes.hasOwnProperty(char)) {
        // if this is a valid character, let's add a character separator
        if (!spacing)
          signals.push(LEN.CHARACTER);
        spacing = false;
      } else {
        // if this is a space, let's add a word separator
        signals.push(LEN.WORD);
        spacing = true;
      }


    if (options.codes.hasOwnProperty(char)) {
      // grab the code
      code = options.codes[char];
      // split the code into signals
      signal = code.split('');
      // iterate through. Remember to toggle on/off for the end of each signal
      for (var i = 0; i < signal.length; i++) {
        if (i != 0)
          signals.push(LEN.DOT);
        if (signal[i] == '.')
          signals.push(LEN.DOT);
        if (signal[i] == '-')
          signals.push(LEN.DASH);
      }
    }
  }

  // Here's our run method with its callback.
  var runner = 0;
  var run = function() {
    options.toggle();
    if (runner == signals.length) {
      return callback();
    }
    signal = signals[runner++];
    options.timeouter(run, signal);
  }
  run();
};

module.exports = transmitter;

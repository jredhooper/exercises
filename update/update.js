var update = function(oldstate, commands) {
  console.log();

  var state;

  state = (function(source) {
    // Base case: state is a number/string
    var clone = source;

    // If the state is a complex object
    // Assign all properties to the object
    if (typeof source === 'object') {
      clone = {};
      Object.assign(clone, source);
    }

    // If it's an Array, clone the array.
    if (Array.isArray(source)) {
      delete clone;
      clone = source.slice(0);
    }

    return clone;
  })(oldstate);


  var functions = {
    $push: function(state, list) {
      // Push every element in the list into the state array.
      list.forEach(function(item) {
        state.push(item);
      });
      return state;
    },
    $unshift: function(state, list) {
      // unshift every element in the list in the state array.
      list.forEach(function(item) {
        state.unshift(item);
      });
      return state;
    },
    $splice: function(state, list) {
      // Apply splice to the state array for each element in the splice array
      list.forEach(function(splicer) {
        state.splice.apply(state, splicer);
      });
      return state;
    },
    $merge: function(state, obj) {
      // Merge the objects based on all their properties
      var keys = Object.keys(obj);
      for (var i = 0; i < keys.length; i++) {
        var key = keys[i];
        state[key] = obj[key];
      }
      return state;
    },
    $set: function(s, set) {
      // Just return what we're setting it to.
      // Discard previous state
      return set;
    },
    $apply: function(state, func) {
      // Apply the func to the state
      return func(state);
    }
  };


  // For each key in "bundle"
  keys = Object.keys(commands)



  // Let's iterate through the state
  for (var i = 0; i < keys.length; i++) {
    var key = keys[i];
    if (functions.hasOwnProperty(key)) {
      return functions[key](state, commands[key]);
    } else {
      state[key] = update(state[key], commands[key]);
    }
  }

  return state;
};


module.exports = update;

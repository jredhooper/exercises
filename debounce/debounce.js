var debounce = function(fn, interval) {
  // Timeout for debounce
  var timeout;

  return function() {
    var ctx = this;
    var args = arguments;

    var run = function(ctx, args) {
      fn.apply(ctx, args);
    }

    // reset after every attempt.
    clearTimeout(timeout);

    // reset timeout after every call
    timeout = setTimeout(function() {
      run(ctx, args);
    }, interval);
  };
};

module.exports = debounce;
